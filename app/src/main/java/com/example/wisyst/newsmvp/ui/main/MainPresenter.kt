package com.example.wisyst.newsmvp.ui.main

/**
 * Created by wisyst on 7/2/2018.
 */
interface MainPresenter {
    fun getData()
}