package com.example.wisyst.newsmvp.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.wisyst.newsmvp.R
import com.example.wisyst.newsmvp.data.remote.Post
import com.example.wisyst.newsmvp.ui.main.NewsAdapter.ViewHolde
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_news.view.*

/**
 * Created by wisyst on 7/4/2018.
 */
class NewsAdapter(val News: List<Post>, var context: Context,
                  val listner: OnItemClickListener) : RecyclerView.Adapter<ViewHolde>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolde {
        val view = LayoutInflater.from(context).inflate(R.layout.item_news, parent, false)
        return ViewHolde(view)
    }

    override fun getItemCount(): Int {
        return News.size
    }

    override fun onBindViewHolder(holder: ViewHolde, position: Int) {
        val post = News.get(position)
        holder?.tvTitel?.text = post.post_title
        holder?.tvBrif?.text = post.content_brief
        Picasso.with(context).load(post.post_image_url).into(holder.image);
        holder.bind(post, position, listner)
    }

    class ViewHolde(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitel = itemView.tv_status
        val tvBrif = itemView.tv_order_date
        val image = itemView.iv_delete_order

        fun bind(item: Post, i: Int, listener: NewsAdapter.OnItemClickListener) {

            itemView.setOnClickListener { listener.onItemClick(item, i) }

        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Post, position: Int)
    }

}


