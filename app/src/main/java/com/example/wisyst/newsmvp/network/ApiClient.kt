package com.example.wisyst.newsmvp.network

import android.content.Context
import com.example.wisyst.newsmvp.Const
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiClient(context: Context) {
    var mcontext: Context

    init {
        this.mcontext = context.applicationContext
    }

    companion object {
         private val REQUEST_TIMEOUT = 60
        private var okHttpClient: OkHttpClient? = null
        private var retrofit: Retrofit? = null

        val client: Retrofit
            get() {
                if (okHttpClient == null)
                    initOkHttp()

                if (retrofit == null) {
                    retrofit = Retrofit.Builder()
                            .baseUrl(Const.BASE_URL)
                            .client(okHttpClient!!)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                }
                return this!!.retrofit!!
            }

        private fun initOkHttp() {
            val httpClient = OkHttpClient().newBuilder()
                    .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            httpClient.addInterceptor(interceptor)

            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Request-Type", "Android")
                        .addHeader("Content-Type", "application/json")

                val url = original.url()
                        .newBuilder()
                        .addQueryParameter(Const.QUERY_PARAM_EMAIL_NAME, Const.QUERY_PARAM_EMAIL_VALUE)
                        .addQueryParameter(Const.QUERY_PARAM_PASSWORD_NAME, Const.QUERY_PARAM_PASSWORD_VALUE)
                        .build()

                val request = requestBuilder.url(url).build()
                chain.proceed(request)
            }

            okHttpClient = httpClient.build()
        }
    }
}
