package com.example.wisyst.newsmvp.data.remote

data class Post(var categoryName: String? = null
                , var postId: String? = null,
                var postType: String? = null,
                var post_title: String? = null,
                var postCreateDate: String? = null,
                var postViewsNumber: String? = null,
                var post_image_url: String? = null,
                var writerName: String? = null,
                var content_brief: String? = null)