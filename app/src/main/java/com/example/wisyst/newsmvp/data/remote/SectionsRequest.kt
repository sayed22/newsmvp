package com.example.wisyst.newsmvp.data.remote

/**
 * Created by wisyst on 3/12/2018.
 */

data class SectionsRequest(val type: String, val id: String )