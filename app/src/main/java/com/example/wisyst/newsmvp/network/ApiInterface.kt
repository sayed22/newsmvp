package com.example.wisyst.newsmvp.network


 import com.example.wisyst.newsmvp.Const
import com.example.wisyst.newsmvp.data.remote.NewsResponce
import com.example.wisyst.newsmvp.data.remote.SectionsRequest
 import retrofit2.Call
 import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {


    @POST(Const.API_GET_SECTIONS)
    fun NEWS_RESPONCE_CALL(@Body sectionsRequest: SectionsRequest): Call<NewsResponce>

}


