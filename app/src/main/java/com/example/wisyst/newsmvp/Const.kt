package com.example.wisyst.newsmvp

/**
 * Created by wisyst on 3/5/2018.
 */

object Const {
    const val SHARED_PREFERENCES_FILE_NAME = "Khaybar"
    const val BASE_URL = "http://khaybarnews.com/api/"
    const val QUERY_PARAM_EMAIL_NAME = "api_email"
    const val QUERY_PARAM_EMAIL_VALUE = "api_email@api_email.com"
    const val QUERY_PARAM_PASSWORD_NAME = "api_password"
    const val QUERY_PARAM_PASSWORD_VALUE = "P@ssww07#d"
    const val API_GET_SECTIONS = "GetCategoryPostsList"
    const val API_GET_POST_DETAILS = "GetPostDetails"
    const val API_ARTICLE_COMMENT = "GetPostComments"

}
