package com.example.wisyst.newsmvp.ui.main

import com.example.wisyst.newsmvp.data.remote.NewsResponce
import com.example.wisyst.newsmvp.data.remote.SectionsRequest
import com.example.wisyst.newsmvp.network.ApiClient
import com.example.wisyst.newsmvp.network.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by wisyst on 7/4/2018.
 */
class MainPresenterImpl(var mainView: MainView) : MainPresenter {

    override fun getData() {
        mainView.showProgress(true)
        val apiInterface = ApiClient.client.create<ApiInterface>(ApiInterface::class.java!!)
        val sectionsRequest = SectionsRequest("1", "3")
        apiInterface.NEWS_RESPONCE_CALL(sectionsRequest).enqueue(object : Callback<NewsResponce> {

            override fun onFailure(call: Call<NewsResponce>?, t: Throwable?) {
                mainView.showProgress(false)
                mainView.showError("Some Thing went wrong")
            }

            override fun onResponse(call: Call<NewsResponce>, response: Response<NewsResponce>) {
                mainView.showProgress(false)
                mainView.getData(response.body())
            }
        })
    }
}