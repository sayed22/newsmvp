package com.example.wisyst.newsmvp.ui.main

import com.example.wisyst.newsmvp.data.remote.NewsResponce

/**
 * Created by wisyst on 7/2/2018.
 */
interface MainView {

    fun showError(message: String);

    fun showProgress(show :Boolean);

    fun getData(data: NewsResponce);
}