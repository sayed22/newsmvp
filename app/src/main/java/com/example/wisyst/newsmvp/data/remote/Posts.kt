package com.example.wisyst.newsmvp.data.remote

data class Posts(
        var currentPage: Int,
        var data: List<Post>,
        var firstPageUrl: String,
        var from: Int,
        var lastPage: Int,
        var lastPageUrl: String,
        var nextPageUrl: String,
        var path: String,
        var perPage: Int,
        var prevPageUrl: Any,
        var to: Int,
        var total: Int
)
