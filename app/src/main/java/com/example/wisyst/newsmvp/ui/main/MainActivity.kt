package com.example.wisyst.newsmvp.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.example.wisyst.newsmvp.R
import com.example.wisyst.newsmvp.data.remote.NewsResponce
import com.example.wisyst.newsmvp.data.remote.Post
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView, NewsAdapter.OnItemClickListener {
    override fun onItemClick(item: Post, position: Int) {
        Toast.makeText(this, "hi", Toast.LENGTH_SHORT).show()
    }

    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter = MainPresenterImpl(this)
        mainPresenter.getData()
    }

    override fun showProgress(show: Boolean) {
        if (show)
            pb_news.visibility = View.VISIBLE
        else
            pb_news.visibility = View.GONE
    }


    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun getData(data: NewsResponce) {
        rv_news.layoutManager = LinearLayoutManager(this)
        rv_news.adapter = NewsAdapter(
                data.posts.data,
                this, this)
    }
}
